import React from "react";

const LibrarySong = ({
  song,
  setCurrentSong,
  songs,
  setSongs,
  setLibraryStatus,
}) => {
  const songSelectHandler = () => {
    setCurrentSong(song);

    setLibraryStatus(false);

    // Update the song status
    setSongs(
      songs.map((s) => {
        return {
          ...s,
          active: s.id === song.id,
        };
      })
    );
  };

  return (
    <div
      className={`library-song ${song.active ? "selected" : ""}`}
      onClick={songSelectHandler}
    >
      <img src={song.cover} alt={song.name}></img>
      <div className="song-description">
        <h3>{song.name}</h3>
        <h4>{song.artist}</h4>
      </div>
    </div>
  );
};

export default LibrarySong;
