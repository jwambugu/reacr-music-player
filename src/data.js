import { v4 as uuidv4 } from "uuid";
function chillHop() {
  return [
    {
      name: "Foto Moto",
      artist: "Foto Moto ft. Noti Flow",
      cover:
        "https://source.boomplaymusic.com/group2/M03/C2/DC/rBEeM169hY6ANBh7AABnnXN-utg739.png",
      id: uuidv4(),
      active: false,
      color: ["#F6CC30", "#F39DE3"],
      audio:
        "https://hearthis.at/citimuzik03/noti-flow-ft-benzema-foto-moto-hearthis.at/stream.mp3?referer=https%3A%2F%2Fwww.citimuzik.com%2F2020%2F05%2Fnoti-flow-ft-benzema-foto-moto.html&s=G3h&t=1606104275",
    },
    {
      name: "Beaver Creek",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/09/0255e8b8c74c90d4a27c594b3452b2daafae608d-1024x1024.jpg",
      artist: "Aso, Middle School, Aviino",
      audio: "https://mp3.chillhop.com/serve.php/?mp3=10075",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: false,
    },
    {
      name: "Daylight",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/07/ef95e219a44869318b7806e9f0f794a1f9c451e4-1024x1024.jpg",
      artist: "Aiguille",
      audio: "https://mp3.chillhop.com/serve.php/?mp3=9272",
      color: ["#EF8EA9", "#ab417f"],
      id: uuidv4(),
      active: false,
    },
    {
      name: "Keep Going",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/07/ff35dede32321a8aa0953809812941bcf8a6bd35-1024x1024.jpg",
      artist: "Swørn",
      audio: "https://mp3.chillhop.com/serve.php/?mp3=9222",
      color: ["#CD607D", "#c94043"],
      id: uuidv4(),
      active: false,
    },
    {
      name: "Nightfall",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/07/ef95e219a44869318b7806e9f0f794a1f9c451e4-1024x1024.jpg",
      artist: "Aiguille",
      audio: "https://mp3.chillhop.com/serve.php/?mp3=9148",
      color: ["#EF8EA9", "#ab417f"],
      id: uuidv4(),
      active: false,
    },
    {
      name: "Reflection",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/07/ff35dede32321a8aa0953809812941bcf8a6bd35-1024x1024.jpg",
      artist: "Swørn",
      audio: "https://mp3.chillhop.com/serve.php/?mp3=9228",
      color: ["#CD607D", "#c94043"],
      id: uuidv4(),
      active: false,
    },
    {
      name: "Under the City Stars",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/09/0255e8b8c74c90d4a27c594b3452b2daafae608d-1024x1024.jpg",
      artist: "Aso, Middle School, Aviino",
      audio: "https://mp3.chillhop.com/serve.php/?mp3=10074",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: false,
    },
    {
      name: "Magenta Forever",
      artist: "Aviiono",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/10/23fdd99adc3e16abcb67b004ea3e748ebf433a49-1024x1024.jpg",
      id: uuidv4(),
      active: false,
      color: ["#274341", "#4C2E52"],
      audio: "https://mp3.chillhop.com/serve.php/?mp3=10458",
    },
    {
      name: "Sodium",
      artist: "Philanthrope, Tesk",
      cover:
        "https://chillhop.com/wp-content/uploads/2020/10/efaa848553d09b4d9fc0752844456e41b34de276-1024x1024.jpg",
      id: uuidv4(),
      active: false,
      color: ["#5BAAF0", "#2A3133"],
      audio: "https://mp3.chillhop.com/serve.php/?mp3=10448",
    },
  ];
}

export default chillHop;
